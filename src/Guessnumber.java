import java.util.Random;
import java.util.Scanner;

public class Guessnumber {
    public static void main(String[] args) {
        Random rand = new Random();
        String playAgain = "";
        do{
            int chances = 7;
            System.out.println("还有"+chances+"次机会，请猜一个1-100之间整数：");
            int answer = rand.nextInt(100)+1;
            Scanner sc = new Scanner(System.in);
            int guess = sc.nextInt();
            while (guess != answer && chances>1){
                chances = chances - 1;
                if(guess>answer){
                    System.out.println(guess+"猜大了! 还有"+chances+"次机会");
                }else if(guess<answer){
                    System.out.println(guess+"猜小了! 还有"+chances+"次机会");
                }
                guess = sc.nextInt();
            }
            if(guess==answer){
                System.out.println("恭喜你猜对了！");
            }
            System.out.println("再玩一次吗？（y/n）？");
            playAgain = sc.next();
        }while(playAgain.equalsIgnoreCase("y"));
    }
}
